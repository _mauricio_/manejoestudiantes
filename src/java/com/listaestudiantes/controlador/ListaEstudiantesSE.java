/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.listaestudiantes.controlador;

import com.listaestudiantes.pojo.Estudiante;
import com.listaestudiantes.pojo.Nodo;
import java.io.Serializable;

/**
 *
 * @author cloaiza
 */
public class ListaEstudiantesSE implements Serializable {

    private Nodo cabeza;

    public ListaEstudiantesSE() {
    }

    public Nodo getCabeza() {
        return cabeza;
    }

    public String adicionarNodo(Estudiante info) {
        //Proxima Clase Disparar Excepciones
        if (this.cabeza == null) {
            cabeza = new Nodo(info);

        } else {
            Nodo temp = cabeza;
            while (temp.getSiguiente() != null) {
                temp = temp.getSiguiente();
            }
            //Parado en el último  
            temp.setSiguiente(new Nodo(info));
        }
        return "Adicionado con éxito";

    }

    public String adicionarNodoInicial(Estudiante info) {
        //Proxima Clase Disparar Excepciones
        if (this.cabeza == null) {
            cabeza = new Nodo(info);

        } else {
            Nodo temp = new Nodo(info);
            temp.setSiguiente(cabeza);
            cabeza = temp;
        }
        return "Adicionado con éxito";

    }

    public String listarNodos() {
        String listado = "";
        if (cabeza == null) {
            return "La lista está vacía";
        } else {
            Nodo temp = cabeza;
            while (temp != null) {
                listado += temp.getDato();
                temp = temp.getSiguiente();
            }
            return listado;
        }

    }

    public int contarNodos() {
        if (cabeza == null) {
            return 0;
        } else {
            Nodo temp = cabeza;
            int cont = 1;
            while (temp != null) {
                cont++;
                temp = temp.getSiguiente();
            }
            return cont;
        }
    }

    public Nodo obtenerUltimoNodo() {
        if (cabeza != null) {
            Nodo temp = cabeza;
            while (temp.getSiguiente() != null) {
                temp = temp.getSiguiente();
            }
            return temp;
        }
        return null;
    }

    public int obtenerUltimoNodoEliminar() {
        if (cabeza != null) {
            int conta = 1;
            Nodo temp = cabeza;
            while (temp.getSiguiente() != null) {
                temp = temp.getSiguiente();
                conta++;
            }
            return conta;
        }
        return 0;
    }

    public Nodo obtenerNodoxPosicion(int pos) {
        if (cabeza != null) {
            Nodo temp = cabeza;
            int cont = 1;
            while (temp != null) {
                if (pos == cont) {

                    return new Nodo(temp.getDato());
                }
                temp = temp.getSiguiente();
                cont++;
            }
        }
        return null;
    }

    /////////////////
    /////////////////
    int contadorQuiz = 1;
    int contadorQuiz_ = 1;

    public String quizPuntoDos() {
        String listado = "";
        String listado_ = "";
        if (cabeza == null) {
            return "La lista está vacía";
        } else {
            Nodo temp = cabeza;
            while (temp != null) {

                if (contadorQuiz % 2 != 0) {
                    listado = listado + "--- Posición Impar #";
                    listado = listado + " " + contadorQuiz;
                    listado += " " + temp.getDato();
                    temp = temp.getSiguiente();

                } else {

                    temp = temp.getSiguiente();

                }

                contadorQuiz++;
            }

            /////////////////////////
            Nodo temp_ = cabeza;
            while (temp_ != null) {

                if (contadorQuiz_ % 2 != 0) {
                    temp_ = temp_.getSiguiente();

                } else {

                    listado_ = listado_ + "--- Posición Par #";
                    listado_ = listado_ + " " + contadorQuiz_;
                    listado_ += " " + temp_.getDato();
                    temp_ = temp_.getSiguiente();

                }

                contadorQuiz_++;
            }

            System.out.println(listado + listado_);
contadorQuiz=1;
contadorQuiz_=1;
            return listado + listado_;
        }

    }

    ///////////////////////
    public Nodo obtenerNodoInvertido(int pos) {

        if (cabeza != null) {
            Nodo temp = cabeza;
            int cont = 1;
            while (temp != null) {
                if (pos == cont) {

                    return new Nodo(temp.getDato());
                }
                temp = temp.getSiguiente();
                cont++;
            }
        }
        return null;
    }

    public String eliminarNodoPorPosicion(int posicion) {
        if (posicion <= 0) {
            return "No existe nodo en esta posición.";
        } else if (posicion == 1) {

            cabeza = cabeza.getSiguiente();
            return "Nodo eliminado exitosamente.";
        } else {
            Nodo temp1 = cabeza;
            int cont = 1;
            while (temp1 != null && cont <= posicion) {
                if (cont == posicion - 1) {
                    Nodo temp2 = temp1;
                    temp1 = temp1.getSiguiente();
                    temp1 = temp1.getSiguiente();
                    temp2.setSiguiente(temp1);
                    temp1 = temp2;
                }
                cont++;
                temp1 = temp1.getSiguiente();
            }
            return "Nodo eliminado satisfactoriamente.";
        }

    }
}
